# OnePlaceIntegrations.NET

A suite of helpers for creating integrations using the One Place AWS REST API.


## Getting Started

## Instantiate REST Client

```csharp
string clientId = ConfigurationManager.AppSettings["OP_CLIENT_ID"];
string clientSecret = ConfigurationManager.AppSettings["OP_CLIENT_SECRET"];
string baseUrl = ConfigurationManager.AppSettings["OP_URL"];
OnePlaceIntegrations.Core.RestClient opRestClient = new OnePlaceIntegrations.Core.RestClient(baseUrl, clientId, clientSecret);
```

## Generate Models

```csharp
OnePlaceIntegrations.ClassBuilder.Factory classBuilder = new OnePlaceIntegrations.ClassBuilder.Factory(opRestClient);
await classBuilder.Build();
```

## GET

```csharp
List<Contact> contacts = await opRestClient.GetAllObjectDataAsync<Contact>();
```

## CREATE
    
```csharp
Contact contact = new Contact();
contact.FirstName = "Test";
contact.LastName = "Test";
contact.Email = "test@intapp.com";
contact.Status = "3265"; //This is a choice field, still have to create a tool for this.
await opRestClient.CreateObjectDataAsync<Contact>(new List<Contact>() { contact });
contact.PropertiesToSave.Add("FirstName");
contact.PropertiesToSave.Add("LastName");
contact.PropertiesToSave.Add("Email");
contact.PropertiesToSave.Add("Status"); 
```

## UPDATE
    
```c#
List<Contact> result = await opRestClient.GetAllObjectDataAsync<Contact>();
Contact contact = result[0];
contact.FirstName = "Test2";
contact.PropertiesToSave.Add("FirstName");
await opRestClient.UpdateObjectDataAsync<Contact>(new List<Contact>() { contact });
```