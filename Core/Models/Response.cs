﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.Core.Models
{
   public class ApiResult<T>
   {
      [JsonProperty("statusCode")]
      public long? StatusCode { get; set; }

      [JsonProperty("message")]
      public string Message { get; set; }

      [JsonProperty("errors")]
      public List<Error> Errors { get; set; }

      [JsonProperty("errorInfo")]
      public ErrorInfo ErrorInfo { get; set; }

      public OPResult<T> Model { get; set; }
   }

   public partial class ErrorInfo
   {
      [JsonProperty("code")]
      public long? Code { get; set; }

      [JsonProperty("fieldId")]
      public long? FieldId { get; set; }

      [JsonProperty("description")]
      public string Description { get; set; }

      [JsonProperty("errorType")]
      public long? ErrorType { get; set; }

      [JsonProperty("erroredValue")]
      public object ErroredValue { get; set; }
   }

   public partial class Error
   {
      [JsonProperty("erorCode")]
      public long? ErorCode { get; set; }

      [JsonProperty("message")]
      public string Message { get; set; }

      [JsonProperty("erroredValue")]
      public object ErroredValue { get; set; }
   }

   public class OPResult<T>
   {
      public long? TotalRecords { get; set; }
      public long? TotalPages { get; set; }
      public List<T> Rows { get; set; }
   }
}
