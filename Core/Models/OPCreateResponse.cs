﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.Core.Models
{
   public class OPCreateResponse
   {
      [JsonProperty("id")]
      public long Id { get; set; }

      [JsonProperty("name")]
      public string Name { get; set; }

      [JsonProperty("created By")]
      public string CreatedBy { get; set; }

      [JsonProperty("modified By")]
      public string ModifiedBy { get; set; }

      [JsonProperty("created Date")]
      public string CreatedDate { get; set; }

      [JsonProperty("modified Date")]
      public string ModifiedDate { get; set; }

      [JsonProperty("reference ids")]
      public Dictionary<string, long> ReferenceIds { get; set; }
   }
}
