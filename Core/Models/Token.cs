﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.Core.Models
{
   public class Token
   {
      private int expiresIn;

      [JsonProperty("access_token")]
      public string AccessToken { get; set; }
      [JsonProperty("expires_in")]
      public int ExpiresIn
      {
         get
         {
            return expiresIn;
         }
         set
         {
            expiresIn = value;
            ExpiresAt = DateTime.UtcNow.AddSeconds(value - 5); //built in buffer for expiration
         }
      }
      [JsonProperty("refresh_token")]
      public string RefreshToken { get; set; }
      [JsonProperty("token_type")]
      public string TokenType { get; set; }

      public DateTime ExpiresAt { get; set; }
      public bool IsExpired()
      {
         return ExpiresAt.CompareTo(DateTime.UtcNow) > 0;
      }
   }
}
