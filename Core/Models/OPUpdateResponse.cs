﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.Core.Models
{
   public class OPUpdateResponse
   {
      [JsonProperty("# records modified")]
      public string NumRecordsModifiedStr { get; set; }
      public long? NumRecordsModified { get; set; }
   }
}
