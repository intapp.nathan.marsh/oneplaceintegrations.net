﻿using OnePlaceIntegrations.Core.Models;
using OnePlaceIntegrations.ClassBuilder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnePlaceIntegrations.Core.Interfaces;
using System.Reflection;
using System.Collections.Specialized;
using System.Web;

namespace OnePlaceIntegrations.Core
{
   public static class Utilities
   {
      #region methods
      public static long? GetListId<T>() where T : IOnePlaceObject, new()
      {
         Type type = typeof(T);
         var model = new T();
         long? listId = type.GetProperty("ListId").GetValue(model) as long?;
         return listId;
      }

      public static long? GetId<T>(T data) where T : IOnePlaceObject, new()
      {
         Type type = typeof(T);
         return type.GetProperty("Id").GetValue(data) as long?;
      }

      public static string GetCsharpProperty(string className, ObjectMetadataField field, Dictionary<string, string> replaceDictionary)
      {
         string csharpProp = Utilities.GetCleanedName(field.Name, replaceDictionary);
         if (csharpProp == className)
         {
            csharpProp = "_" + csharpProp;
         }

         return csharpProp;
      }

      public static string GetJsonProperty(string className, ObjectMetadataField field)
      {
         return Char.ToLowerInvariant(field.Name[0]) + field.Name.Substring(1);
      }

      public static string ReplaceInvalidCharacters(string inputString, Dictionary<string, string> replaceDictionary)
      {
         foreach (string symbol in replaceDictionary.Keys)
         {
            if (inputString.Contains(symbol))
            {
               inputString = inputString.Replace(symbol, replaceDictionary[symbol]);
            }
         }

         foreach (char character in inputString.Where(x => !char.IsLetterOrDigit(x)).Distinct())
         {
            inputString = inputString.Replace(character, ' ');
         }

         return inputString;
      }


      public static string GetCleanedName(string inputString, Dictionary<string, string> replaceDictionary)
      {
         inputString = ReplaceInvalidCharacters(inputString, replaceDictionary).ToLower();

         string[] tokens = inputString.Split(' ');
         string cleanedString = string.Empty;

         foreach (string token in tokens)
         {
            if (string.IsNullOrWhiteSpace(token)) continue;

            cleanedString += char.ToUpper(token[0]) + string.Join(string.Empty, token.Skip(1));
         }

         if (cleanedString.Length > 0 && !char.IsLetter(cleanedString, 0))
         {
            cleanedString = cleanedString.Insert(0, "_");
         }

         return cleanedString;
      }

      public static bool IsAnyNullOrEmpty(object obj)
      {
         if (Object.ReferenceEquals(obj, null))
            return true;

         return obj.GetType().GetProperties()
             .Any(x => IsNullOrEmpty(x.GetValue(obj)));
      }

      public static bool IsNullOrEmpty(object value)
      {
         if (Object.ReferenceEquals(value, null))
            return true;

         var type = value.GetType();
         return type.IsValueType
             && Object.Equals(value, Activator.CreateInstance(type));
      }
      #endregion



   }
}
