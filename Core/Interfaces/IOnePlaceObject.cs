﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.Core.Interfaces
{
   public interface IOnePlaceObject
   {
      string ListName { get; }
      long ListId { get; }
      List<string> PropertiesToSave { get; }
      Dictionary<string, long> ReferenceIds { get; set; }
      Dictionary<string, string> Links { get; set; }
   }
}
