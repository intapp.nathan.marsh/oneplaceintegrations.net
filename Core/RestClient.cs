﻿#region using
using Newtonsoft.Json;
using OnePlaceIntegrations.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using OnePlaceIntegrations.Core.Interfaces;
using OnePlaceIntegrations.ClassBuilder.Models;
using System.Collections.Specialized;
using OnePlaceHelpers.Core;
#endregion

namespace OnePlaceIntegrations.Core
{
   public class RestClient
   {
      #region private properties
      private string clientId;
      private string clientSecret;
      private Token accessToken;
 

      private JsonSerializerSettings settings = new JsonSerializerSettings
      {
         NullValueHandling = NullValueHandling.Ignore,
         MissingMemberHandling = MissingMemberHandling.Ignore,
         DateTimeZoneHandling = DateTimeZoneHandling.Utc
      };
      #endregion

      #region public properties
      public HttpClient _HttpClient { get; set; }
      public string SiteUrl { get; set; }
      #endregion
      public RestClient(string siteUrl, string clientId, string clientSecret)
      {
         SiteUrl = GetSiteUrl(siteUrl);
         this.clientId = clientId;
         this.clientSecret = clientSecret;
         _HttpClient = new HttpClient();
         accessToken = GetAccessTokenAsync().Result;
         SetAuthorizationHeader(accessToken);
      }

      #region private methods
      private async Task<Token> GetAccessTokenAsync()
      {
         string tokenUrl = SiteUrl + "api/rest/v1/oauth/token";
         var formContent = new FormUrlEncodedContent(new[]
         {
            new KeyValuePair<string, string>("scope", "api"),
            new KeyValuePair<string, string>("grant_type", "client_credentials"),
            new KeyValuePair<string, string>("client_id", clientId),
            new KeyValuePair<string, string>("client_secret", clientSecret)
         });


         HttpResponseMessage response = await _HttpClient.PostAsync(tokenUrl, formContent);
         string json = await response.Content.ReadAsStringAsync();
         Token token = JsonConvert.DeserializeObject<Token>(json, settings);

         return token;
      }
      private string GetSiteUrl(string siteUrl)
      {
         string temp = siteUrl;
         if (!temp.EndsWith("/"))
         {
            temp += "/";
         }

         return temp;
      }
      private async Task UpdateAuthorizationAsync()
      {
         Token token = await GetAccessTokenAsync();
         SetAuthorizationHeader(token);
      }
      #endregion

      #region public methods
      public void SetAuthorizationHeader(Token token, HttpClient httpClient = null)
      {
         if (httpClient == null)
         {
            httpClient = _HttpClient;
         }
         string authHeader = "Authorization";
         if (httpClient.DefaultRequestHeaders.Contains(authHeader))
            httpClient.DefaultRequestHeaders.Remove(authHeader);
         httpClient.DefaultRequestHeaders.TryAddWithoutValidation(authHeader, $"Bearer {token.AccessToken}");

      }

      public async Task<T> GetAsync<T>(string path, int limit = 100, int skip = 0)
      {

         string url = SiteUrl + path;
         HttpResponseMessage response = await _HttpClient.GetAsync(url);
         if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
         {
            await UpdateAuthorizationAsync();
            response = await _HttpClient.GetAsync(url);
         }

         string json = await response.Content.ReadAsStringAsync();
         T result = JsonConvert.DeserializeObject<T>(json);

         return result;
      }

      public async Task<T> PostAsync<T>(string path, object data)
      {
         string url = SiteUrl + path;
         StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
         HttpResponseMessage response = await _HttpClient.PostAsync(url, content);
         if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
         {
            content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            await UpdateAuthorizationAsync();
            response = await _HttpClient.PostAsync(url, content);
         }

         string json = await response.Content.ReadAsStringAsync();
         T result = JsonConvert.DeserializeObject<T>(json);

         return result;
      }

      public async Task<T> PatchAsync<T>(string path, object data)
      {
         string url = SiteUrl + path;
         HttpMethod method = new HttpMethod("PATCH");
         StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
         HttpRequestMessage request = new HttpRequestMessage(method, url) { Content = content };
         HttpResponseMessage response = await _HttpClient.SendAsync(request);
         if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
         {
            await UpdateAuthorizationAsync();
            content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            request = new HttpRequestMessage(method, url) { Content = content };
            response = await _HttpClient.SendAsync(request);
         }

         string json = await response.Content.ReadAsStringAsync();
         T result = JsonConvert.DeserializeObject<T>(json);

         return result;
      }

      public async Task<T> DeleteAsync<T>(string path)
      {
         string url = SiteUrl + path;
         HttpResponseMessage response = await _HttpClient.DeleteAsync(url);
         if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
         {
            await UpdateAuthorizationAsync();
            response = await _HttpClient.DeleteAsync(url);
         }

         string json = await response.Content.ReadAsStringAsync();
         T result = JsonConvert.DeserializeObject<T>(json);

         return result;
      }

      public async Task<List<T>> GetObjectDataAsync<T>(int limit = 100, int skip = 0, string query = "") where T : IOnePlaceObject, new()
      {
         string path = "api/rest/v1/data/" + Utilities.GetListId<T>() + query;
         NameValueCollection nvc = new NameValueCollection();
         nvc.Add("limit", limit.ToString());
         nvc.Add("skip", skip.ToString());
         nvc.Add("showReferences", "true");
         nvc.Add("showLinks", "true");
         path += QueryBuilder.ToQueryString(nvc);
         var result = await GetAsync<List<T>>(path);
         return result;

      }

      public async Task<List<T>> GetAllObjectDataAsync<T>(string query = "") where T : IOnePlaceObject, new()
      {
         List<T> allObjects = new List<T>();
         List<T> responseObjects = new List<T>();
         int limit = 100;
         int skip = 0;
         do
         {
            responseObjects = await GetObjectDataAsync<T>(limit, skip);
            allObjects.AddRange(responseObjects);
            skip += limit;
         } while (responseObjects.Count() > 0);
      
         return allObjects;

      }

      public async Task<List<OPCreateResponse>> CreateObjectDataAsync<T>(List<T> data) where T : IOnePlaceObject, new()
      {
         string path = "api/rest/v1/data/" + Utilities.GetListId<T>();
         List<OPCreateResponse> results = new List<OPCreateResponse>();
         foreach (T item in data)
         {
            OPCreateResponse result = await PostAsync<OPCreateResponse>(path, item);
            results.Add(result);
         }

         return results;
      }

      public async Task<List<OPUpdateResponse>> UpdateObjectDataAsync<T>(List<T> data) where T : IOnePlaceObject, new()
      {

         string path = "";
         List<OPUpdateResponse> results = new List<OPUpdateResponse>();
         foreach (T item in data)
         {
            path = "api/rest/v1/data/" + Utilities.GetListId<T>() + "/" + Utilities.GetId<T>(item);
            OPUpdateResponse result = await PatchAsync<OPUpdateResponse>(path, item);
            results.Add(result);
         }

         OPUpdateResponse aggregated = new OPUpdateResponse();
         aggregated.NumRecordsModified = 0;

         foreach (OPUpdateResponse response in results)
         {
            int numModified = 0;
            if (int.TryParse(response.NumRecordsModifiedStr, out numModified))
            {
               aggregated.NumRecordsModified += numModified;
            }
         }

         aggregated.NumRecordsModifiedStr = aggregated.ToString();

         return results;
      }

      //public async Task<T> Get<T>(int limit = 100, int skip = 0, string query = "")
      //{
      //   T item = default(T);
      //   string url = SiteUrl + "data/" + typeof(T).GetProperty("listName").GetValue(item);
      //   HttpResponseMessage response = await _HttpClient.GetAsync(url);


      //   return item;
      //}
      #endregion
   }
}
