//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Newtonsoft.Json;
//using OnePlaceIntegrations.Core.Interfaces;
//namespace OnePlace.Models
//{
//   public class Contact : IOnePlaceObject
//   {

//      //This will be used allong with ShouldSerialize, so that you only send the data that you need to save and don't accidently delete or overwrite something.
//      //Add the name of the CSharpProperties that you wish to save. e.g. "FirstName"
//      [JsonIgnore]
//      public List<string> PropertiesToSave { get; } = new List<string>();
//      [JsonIgnore]
//      public string ListName { get; } = "Contacts";
//      [JsonIgnore]
//      public long ListId { get; } = 2011;
//      [JsonProperty("id")]
//      public long? Id { get; set; }
//      [JsonProperty("reference ids")]
//      public Dictionary<string, long> ReferenceIds { get; set; } = new Dictionary<string, long>();
//      [JsonProperty("links")]
//      public Dictionary<string, string> Links { get; set; } = new Dictionary<string, string>();
//      [JsonProperty("full Name")]
//      public string FullName { get; set; }

//      [JsonProperty("additional Offices")]
//      public string AdditionalOffices { get; set; }

//      [JsonProperty("alumni")]
//      public string Alumni { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> AlumniChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1499 }, { "No", 1500 } };

//      [JsonProperty("alumniCount")]
//      public string Alumnicount { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> AlumnicountChoiceOptions = new Dictionary<string, long?>() { { "Alumni", 1506 }, { "Not Alumni", 1507 } };

//      [JsonProperty("areas of Interest (AOI) EXPLICITLY SUBSCRIBED")]
//      public string AreasOfInterestAoiExplicitlySubscribed { get; set; }

//      [JsonProperty("areas of Interest (AOI) EXPLICITLY UNSUBSCRIBED")]
//      public string AreasOfInterestAoiExplicitlyUnsubscribed { get; set; }

//      [JsonProperty("areas of Law")]
//      public string AreasOfLaw { get; set; }

//      [JsonProperty("bounce Count")]
//      public decimal? BounceCount { get; set; }

//      [JsonProperty("bounce Rate")]
//      public decimal? BounceRate { get; set; }

//      [JsonProperty("bounced Email")]
//      public string BouncedEmail { get; set; }

//      [JsonProperty("bounced Email Reason")]
//      public string BouncedEmailReason { get; set; }

//      [JsonProperty("business Address City DO NOT USE")]
//      public string BusinessAddressCityDoNotUse { get; set; }

//      [JsonProperty("business Address Country DO NOT USE")]
//      public string BusinessAddressCountryDoNotUse { get; set; }

//      [JsonProperty("business Address Postal Code DO NOT USE")]
//      public string BusinessAddressPostalCodeDoNotUse { get; set; }

//      [JsonProperty("business Address State DO NOT USE")]
//      public string BusinessAddressStateDoNotUse { get; set; }

//      [JsonProperty("business Address Street DO NOT USE")]
//      public string BusinessAddressStreetDoNotUse { get; set; }

//      [JsonProperty("california Data Regime")]
//      public string CaliforniaDataRegime { get; set; }

//      [JsonProperty("click Rate")]
//      public decimal? ClickRate { get; set; }

//      [JsonProperty("communication Preferences")]
//      public string CommunicationPreferences { get; set; }

//      [JsonProperty("company Address Location (Test)")]
//      public string CompanyAddressLocationTest { get; set; }

//      [JsonProperty("company Name")]
//      public string CompanyName { get; set; }

//      [JsonProperty("consent Comments")]
//      public string ConsentComments { get; set; }

//      [JsonProperty("consent Methods")]
//      public string ConsentMethods { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ConsentMethodsChoiceOptions = new Dictionary<string, long?>() { { "Business Card", 1330 }, { "CASL Outreach", 1331 }, { "Conspicuous", 1332 }, { "Direct Email", 1333 }, { "Event Registration", 1334 }, { "Existing Business Relationship", 1335 }, { "Inquiry", 1336 }, { "Other", 1337 }, { "Subscription Updates", 1338 }, { "Website Subscription", 1339 } };

//      [JsonProperty("contact Address City")]
//      public string ContactAddressCity { get; set; }

//      [JsonProperty("contact Address Country")]
//      public string ContactAddressCountry { get; set; }

//      [JsonProperty("contact Address Postal Code")]
//      public string ContactAddressPostalCode { get; set; }

//      [JsonProperty("contact Address State/Province")]
//      public string ContactAddressStateProvince { get; set; }

//      [JsonProperty("contact Address Street")]
//      public string ContactAddressStreet { get; set; }

//      [JsonProperty("contact consent status")]
//      public string ContactConsentStatus { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ContactConsentStatusChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1440 }, { "No", 1441 } };

//      [JsonProperty("cost All Events YTD")]
//      public decimal? CostAllEventsYtd { get; set; }

//      [JsonProperty("count of Firm")]
//      public decimal? CountOfFirm { get; set; }

//      [JsonProperty("data Regime Assigned")]
//      public string DataRegimeAssigned { get; set; }

//      [JsonProperty("data Regime Region")]
//      public string DataRegimeRegion { get; set; }

//      [JsonProperty("date/Time of Last Campaign Attempted")]
//      public DateTime? DateTimeOfLastCampaignAttempted { get; set; }

//      [JsonProperty("date/Time of Last Campaign Delivered")]
//      public DateTime? DateTimeOfLastCampaignDelivered { get; set; }

//      [JsonProperty("date/Time of Last Click")]
//      public DateTime? DateTimeOfLastClick { get; set; }

//      [JsonProperty("date/Time of Last Open")]
//      public DateTime? DateTimeOfLastOpen { get; set; }

//      [JsonProperty("days Until Expiry")]
//      public decimal? DaysUntilExpiry { get; set; }

//      [JsonProperty("department")]
//      public string Department { get; set; }

//      [JsonProperty("description")]
//      public string Description { get; set; }

//      [JsonProperty("direct Business Phone")]
//      public string DirectBusinessPhone { get; set; }

//      [JsonProperty("education")]
//      public string Education { get; set; }

//      [JsonProperty("email")]
//      public string Email { get; set; }

//      [JsonProperty("email 2")]
//      public string Email2 { get; set; }

//      [JsonProperty("employee Status")]
//      public string EmployeeStatus { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> EmployeeStatusChoiceOptions = new Dictionary<string, long?>() { { "Active", 1340 }, { "On Leave", 1341 }, { "Terminated", 1342 } };

//      [JsonProperty("employee Type")]
//      public string EmployeeType { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> EmployeeTypeChoiceOptions = new Dictionary<string, long?>() { { "Regular", 1343 }, { "Temporary", 1344 }, { "Retired", 1345 } };

//      [JsonProperty("events Attended YTD")]
//      public decimal? EventsAttendedYtd { get; set; }

//      [JsonProperty("events Invited YTD")]
//      public decimal? EventsInvitedYtd { get; set; }

//      [JsonProperty("expertise")]
//      public string Expertise { get; set; }

//      [JsonProperty("expressed Consent - Days Since Consent")]
//      public decimal? ExpressedConsentDaysSinceConsent { get; set; }

//      [JsonProperty("expressed Consent - Days Until Expiry")]
//      public decimal? ExpressedConsentDaysUntilExpiry { get; set; }

//      [JsonProperty("expressed Consent Valid")]
//      public string ExpressedConsentValid { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ExpressedConsentValidChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1436 }, { "No", 1437 } };

//      [JsonProperty("expressed Contact Consent")]
//      public string ExpressedContactConsent { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ExpressedContactConsentChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1350 }, { "No", 1351 } };

//      [JsonProperty("expressed Contact Consent Date")]
//      public DateTime? ExpressedContactConsentDate { get; set; }

//      [JsonProperty("fax")]
//      public string Fax { get; set; }

//      [JsonProperty("firm Employee?")]
//      public string FirmEmployee { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> FirmEmployeeChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1035 }, { "No", 1221 } };

//      [JsonProperty("firm Role")]
//      public string FirmRole { get; set; }

//      [JsonProperty("first Name")]
//      public string FirstName { get; set; }

//      [JsonProperty("gender")]
//      public string Gender { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> GenderChoiceOptions = new Dictionary<string, long?>() { { "Male", 1356 }, { "Female", 1357 }, { "Other", 1358 } };

//      [JsonProperty("globally Unsubscribed?")]
//      public string GloballyUnsubscribed { get; set; }

//      [JsonProperty("groups Unsubscribed From")]
//      public string GroupsUnsubscribedFrom { get; set; }

//      [JsonProperty("hire Date")]
//      public DateTime? HireDate { get; set; }

//      [JsonProperty("home Phone")]
//      public string HomePhone { get; set; }

//      [JsonProperty("implied Consent - Days Since Consent")]
//      public decimal? ImpliedConsentDaysSinceConsent { get; set; }

//      [JsonProperty("implied Consent - Days Until Expiry")]
//      public decimal? ImpliedConsentDaysUntilExpiry { get; set; }

//      [JsonProperty("implied Consent Valid")]
//      public string ImpliedConsentValid { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ImpliedConsentValidChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1438 }, { "No", 1439 } };

//      [JsonProperty("implied Contact Consent")]
//      public string ImpliedContactConsent { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> ImpliedContactConsentChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1348 }, { "No", 1349 } };

//      [JsonProperty("implied Contact Consent Date")]
//      public DateTime? ImpliedContactConsentDate { get; set; }

//      [JsonProperty("internal User")]
//      public string InternalUser { get; set; }

//      [JsonProperty("is an Employee?")]
//      public string IsAnEmployee { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> IsAnEmployeeChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1346 }, { "No", 1347 } };

//      [JsonProperty("job Title")]
//      public string JobTitle { get; set; }

//      [JsonProperty("key")]
//      public string Key { get; set; }

//      [JsonProperty("language(s)")]
//      public string Language { get; set; }

//      [JsonProperty("last Activity")]
//      public string LastActivity { get; set; }

//      [JsonProperty("last Activity Date")]
//      public DateTime? LastActivityDate { get; set; }

//      [JsonProperty("last Name")]
//      public string LastName { get; set; }

//      [JsonProperty("last Work Date")]
//      public DateTime? LastWorkDate { get; set; }

//      [JsonProperty("linkedIn URL")]
//      public string LinkedinUrl { get; set; }

//      [JsonProperty("mailing Address")]
//      public string MailingAddress { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> MailingAddressChoiceOptions = new Dictionary<string, long?>() { { "Business Address", 1442 }, { "Personal Address", 1443 } };

//      [JsonProperty("mailing City")]
//      public string MailingCity { get; set; }

//      [JsonProperty("mailing Country")]
//      public string MailingCountry { get; set; }

//      [JsonProperty("mailing Postal Code")]
//      public string MailingPostalCode { get; set; }

//      [JsonProperty("mailing State/Province")]
//      public string MailingStateProvince { get; set; }

//      [JsonProperty("mailing Street Address")]
//      public string MailingStreetAddress { get; set; }

//      [JsonProperty("marketing Subscriber")]
//      public string MarketingSubscriber { get; set; }

//      [JsonProperty("memberships")]
//      public string Memberships { get; set; }

//      [JsonProperty("middle Name")]
//      public string MiddleName { get; set; }

//      [JsonProperty("missing Company Name")]
//      public string MissingCompanyName { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> MissingCompanyNameChoiceOptions = new Dictionary<string, long?>() { { "Missing Company Name", 1238 } };

//      [JsonProperty("missing Email")]
//      public string MissingEmail { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> MissingEmailChoiceOptions = new Dictionary<string, long?>() { { "Missing Email", 1237 } };

//      [JsonProperty("missing Fields")]
//      public string MissingFields { get; set; }

//      [JsonProperty("missing Job Title")]
//      public string MissingJobTitle { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> MissingJobTitleChoiceOptions = new Dictionary<string, long?>() { { "Missing Job Title", 1236 } };

//      [JsonProperty("mobile Phone")]
//      public string MobilePhone { get; set; }

//      [JsonProperty("network ID")]
//      public string NetworkId { get; set; }

//      [JsonProperty("normalized Job Level")]
//      public string NormalizedJobLevel { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> NormalizedJobLevelChoiceOptions = new Dictionary<string, long?>() { { "High", 1466 }, { "Medium", 1467 }, { "Not Mapped", 1468 }, { "Low", 1469 } };

//      [JsonProperty("normalized Job Title")]
//      public string NormalizedJobTitle { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> NormalizedJobTitleChoiceOptions = new Dictionary<string, long?>() {{"Not Mapped",1359},{"C-Suite: Chief Executives/Managing Directors/Presidents/Owners",1360},{"Knowledge
//  Management",1361},{"C-Suite: Chief Information
//  Officers/Technology Directors",1362},{"IT:
//  Professionals",1363},{"Assistant",1364},{"C-Suite: Chief Product Officer",1365},{"Project Managers",1366},{"General",1367},{"HR: General",1368},{"Procurement:
//  Professionals",1369},{"Project
//  Managers",1370},{"C-Suite",1371},{"Occupational
//  Health &amp; Safety Professionals",1372},{"Director: General",1373},{"Lead",1374},{"Accountants",1375},{"C-Suite: Chief Underwriting Officers",1376},{"C-Suite: Chief Risk Directors",1377},{"C-Suite: Chief Compiance Officers",1378},{"HR: Head of HR",1379},{"Legal: In
//  House Lawyers",1380},{"Partner",1381},{"C-Suite: Chief Financial Officers/Finance
//  Directors",1382},{"Architects",1383},{"Coordinator",1384},{"C-Suite: Chief Knowledge Directors",1385},{"Associate",1386},{"Clerk",1387},{"Bankers: Investment Bankers",1388},{"Barristers/Queens Counsel",1389},{"Board: Chair",1390},{"C-Suite: Chief Security Officers",1391},{"Credit: Professionals",1392},{"Finance: Employee",1393},{"Students",1394},{"C-Suite: Chief Data Officers",1395},{"Executive Vice
//  President",1396},{"C-Suite: Chief Strategy Officers",1397},{"C-Suite: Chief Procurement Directors",1398},{"Private Equity
//  Specialists",1399},{"C-Suite: Chief Compliance Officers",1400},{"C-Suite: Chief Operating Officers",1401},{"Administrative",1402},{"Analyst",1403},{"Journalists/Media
//  Contacts",1404},{"C-Suite: Chief Administration Officers",1405},{"Risk Managers",1406},{"Research: Professionals",1407},{"Officer",1408},{"Executive",1409},{"Manager",1410},{"Legal: Chief
//  Legal Officers/General Counsel",1411},{"Environmental Consultants",1412},{"Operations",1413},{"C-Suite: Chief Marketing
//  Officers/Marketing Directors",1414},{"C-Suite: Chief Revenue Officers",1415},{"Senior Vice
//  President",1416},{"C-Suite: Chief Underwriting Directors",1417},{"Tax:
//  Professionals",1418},{"Supplier
//  Contacts",1419},{"Government:
//  Ministers/Politicians/Members of Parliament",1420},{"Government:
//  Employees",1421},{"Board: Non-Executive Directors/Other
//  Board Directors",1422},{"Engineers",1423},{"Consultant",1424},{"Secretary",1425},{"Senior Manager",1426},{"Assistant Manager",1427},{"HR:
//  Professionals",1428},{"Marketing:
//  Marketing Professionals",1429},{"Academics",1430},{"Vice President",1431},{"C-Suite: Chief Audit/Accounting Officers",1432},{"Paralegal",1433},{"Tax: Heads of
//  Tax",1434},{"Billing Contacts",1435},{"Marketing: Marketing Professionals",3287},{"Senior Vice President",1508}};

//      [JsonProperty("notes")]
//      public string Notes { get; set; }

//      [JsonProperty("office Location")]
//      public string OfficeLocation { get; set; }

//      [JsonProperty("open Rate")]
//      public decimal? OpenRate { get; set; }

//      [JsonProperty("other Phone")]
//      public string OtherPhone { get; set; }

//      [JsonProperty("person")]
//      public string Person { get; set; }

//      [JsonProperty("person ID")]
//      public string PersonId { get; set; }

//      [JsonProperty("personal Email")]
//      public string PersonalEmail { get; set; }

//      [JsonProperty("photo")]
//      public object Photo { get; set; }

//      [JsonProperty("practice Area")]
//      public string PracticeArea { get; set; }

//      [JsonProperty("practice Areas")]
//      public string PracticeAreas { get; set; }

//      [JsonProperty("preferred Name")]
//      public string PreferredName { get; set; }

//      [JsonProperty("prefix")]
//      public string Prefix { get; set; }

//      [JsonProperty("primary Office")]
//      public string PrimaryOffice { get; set; }

//      [JsonProperty("professional Certifications")]
//      public string ProfessionalCertifications { get; set; }

//      [JsonProperty("publications Sent YTD")]
//      public decimal? PublicationsSentYtd { get; set; }

//      [JsonProperty("rehire Date")]
//      public DateTime? RehireDate { get; set; }

//      [JsonProperty("relationship Co-Owners")]
//      public string RelationshipCoOwners { get; set; }

//      [JsonProperty("relationship Owners")]
//      public string RelationshipOwners { get; set; }

//      [JsonProperty("relationships Signature Block")]
//      public string RelationshipsSignatureBlock { get; set; }

//      [JsonProperty("salutation")]
//      public string Salutation { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> SalutationChoiceOptions = new Dictionary<string, long?>() { { "Mr.", 1493 }, { "Mrs.", 1494 }, { "Ms.", 1495 }, { "Dr.", 1496 }, { "Hon.", 1497 }, { "Prof.", 1498 } };

//      [JsonProperty("start Date")]
//      public DateTime? StartDate { get; set; }

//      [JsonProperty("status")]
//      public string Status { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> StatusChoiceOptions = new Dictionary<string, long?>() { { "Current&#13;", 3264 }, { "On Leave&#13;", 3265 }, { "Retired&#13;", 3266 }, { "Deceased&#13;", 3267 }, { "Archived&#13;", 3268 } };

//      [JsonProperty("suffix")]
//      public string Suffix { get; set; }

//      [JsonProperty("termination Date")]
//      public DateTime? TerminationDate { get; set; }

//      [JsonProperty("test")]
//      public string Test { get; set; }

//      [JsonProperty("timekeeper ID")]
//      public string TimekeeperId { get; set; }

//      [JsonProperty("total Clicks")]
//      public decimal? TotalClicks { get; set; }

//      [JsonProperty("total Cost of All Events")]
//      public decimal? TotalCostOfAllEvents { get; set; }

//      [JsonProperty("total Emails Attempted")]
//      public decimal? TotalEmailsAttempted { get; set; }

//      [JsonProperty("total Emails Received")]
//      public decimal? TotalEmailsReceived { get; set; }

//      [JsonProperty("total Events Attended")]
//      public decimal? TotalEventsAttended { get; set; }

//      [JsonProperty("total Events Invited")]
//      public decimal? TotalEventsInvited { get; set; }

//      [JsonProperty("total Opens")]
//      public decimal? TotalOpens { get; set; }

//      [JsonProperty("total Publications Sent")]
//      public decimal? TotalPublicationsSent { get; set; }

//      [JsonProperty("type")]
//      public string Type { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> TypeChoiceOptions = new Dictionary<string, long?>() { { "Personnel", 1321 }, { "Client Personnel", 1322 }, { "Consultants", 1323 }, { "Service Providers", 1324 }, { "Corporate Counsel", 1325 }, { "Court Personnel", 1326 }, { "Alumni", 1327 }, { "Media/Press Contacts", 1328 }, { "Law Students", 1329 } };

//      [JsonProperty("unique Clicks")]
//      public decimal? UniqueClicks { get; set; }

//      [JsonProperty("unique Opens")]
//      public decimal? UniqueOpens { get; set; }

//      [JsonProperty("unsubscribe All")]
//      public string UnsubscribeAll { get; set; }

//      [JsonIgnore]
//      public Dictionary<string, long?> UnsubscribeAllChoiceOptions = new Dictionary<string, long?>() { { "Yes", 1352 }, { "No", 1353 } };

//      [JsonProperty("unsubscribe Count")]
//      public decimal? UnsubscribeCount { get; set; }

//      [JsonProperty("unsubscribe Rate")]
//      public decimal? UnsubscribeRate { get; set; }

//      [JsonProperty("created By")]
//      public string CreatedBy { get; set; }

//      [JsonProperty("modified By")]
//      public string ModifiedBy { get; set; }

//      [JsonProperty("created Date")]
//      public DateTime? CreatedDate { get; set; }

//      [JsonProperty("modified Date")]
//      public DateTime? ModifiedDate { get; set; }

//      //These methods are used by the One Place Client to send only the data that is updated, using the PropertiesToSave list.
//      public bool ShouldSerializeFullName()
//      {
//         return PropertiesToSave.Contains("FullName") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAdditionalOffices()
//      {
//         return PropertiesToSave.Contains("AdditionalOffices") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAlumni()
//      {
//         return PropertiesToSave.Contains("Alumni") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAlumnicount()
//      {
//         return PropertiesToSave.Contains("Alumnicount") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAreasOfInterestAoiExplicitlySubscribed()
//      {
//         return PropertiesToSave.Contains("AreasOfInterestAoiExplicitlySubscribed") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAreasOfInterestAoiExplicitlyUnsubscribed()
//      {
//         return PropertiesToSave.Contains("AreasOfInterestAoiExplicitlyUnsubscribed") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeAreasOfLaw()
//      {
//         return PropertiesToSave.Contains("AreasOfLaw") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBounceCount()
//      {
//         return PropertiesToSave.Contains("BounceCount") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBounceRate()
//      {
//         return PropertiesToSave.Contains("BounceRate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBouncedEmail()
//      {
//         return PropertiesToSave.Contains("BouncedEmail") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBouncedEmailReason()
//      {
//         return PropertiesToSave.Contains("BouncedEmailReason") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBusinessAddressCityDoNotUse()
//      {
//         return PropertiesToSave.Contains("BusinessAddressCityDoNotUse") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBusinessAddressCountryDoNotUse()
//      {
//         return PropertiesToSave.Contains("BusinessAddressCountryDoNotUse") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBusinessAddressPostalCodeDoNotUse()
//      {
//         return PropertiesToSave.Contains("BusinessAddressPostalCodeDoNotUse") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBusinessAddressStateDoNotUse()
//      {
//         return PropertiesToSave.Contains("BusinessAddressStateDoNotUse") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeBusinessAddressStreetDoNotUse()
//      {
//         return PropertiesToSave.Contains("BusinessAddressStreetDoNotUse") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCaliforniaDataRegime()
//      {
//         return PropertiesToSave.Contains("CaliforniaDataRegime") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeClickRate()
//      {
//         return PropertiesToSave.Contains("ClickRate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCommunicationPreferences()
//      {
//         return PropertiesToSave.Contains("CommunicationPreferences") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCompanyAddressLocationTest()
//      {
//         return PropertiesToSave.Contains("CompanyAddressLocationTest") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCompanyName()
//      {
//         return PropertiesToSave.Contains("CompanyName") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeConsentComments()
//      {
//         return PropertiesToSave.Contains("ConsentComments") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeConsentMethods()
//      {
//         return PropertiesToSave.Contains("ConsentMethods") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactAddressCity()
//      {
//         return PropertiesToSave.Contains("ContactAddressCity") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactAddressCountry()
//      {
//         return PropertiesToSave.Contains("ContactAddressCountry") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactAddressPostalCode()
//      {
//         return PropertiesToSave.Contains("ContactAddressPostalCode") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactAddressStateProvince()
//      {
//         return PropertiesToSave.Contains("ContactAddressStateProvince") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactAddressStreet()
//      {
//         return PropertiesToSave.Contains("ContactAddressStreet") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeContactConsentStatus()
//      {
//         return PropertiesToSave.Contains("ContactConsentStatus") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCostAllEventsYtd()
//      {
//         return PropertiesToSave.Contains("CostAllEventsYtd") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCountOfFirm()
//      {
//         return PropertiesToSave.Contains("CountOfFirm") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDataRegimeAssigned()
//      {
//         return PropertiesToSave.Contains("DataRegimeAssigned") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDataRegimeRegion()
//      {
//         return PropertiesToSave.Contains("DataRegimeRegion") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDateTimeOfLastCampaignAttempted()
//      {
//         return PropertiesToSave.Contains("DateTimeOfLastCampaignAttempted") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDateTimeOfLastCampaignDelivered()
//      {
//         return PropertiesToSave.Contains("DateTimeOfLastCampaignDelivered") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDateTimeOfLastClick()
//      {
//         return PropertiesToSave.Contains("DateTimeOfLastClick") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDateTimeOfLastOpen()
//      {
//         return PropertiesToSave.Contains("DateTimeOfLastOpen") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDaysUntilExpiry()
//      {
//         return PropertiesToSave.Contains("DaysUntilExpiry") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDepartment()
//      {
//         return PropertiesToSave.Contains("Department") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDescription()
//      {
//         return PropertiesToSave.Contains("Description") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeDirectBusinessPhone()
//      {
//         return PropertiesToSave.Contains("DirectBusinessPhone") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEducation()
//      {
//         return PropertiesToSave.Contains("Education") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEmail()
//      {
//         return PropertiesToSave.Contains("Email") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEmail2()
//      {
//         return PropertiesToSave.Contains("Email2") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEmployeeStatus()
//      {
//         return PropertiesToSave.Contains("EmployeeStatus") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEmployeeType()
//      {
//         return PropertiesToSave.Contains("EmployeeType") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEventsAttendedYtd()
//      {
//         return PropertiesToSave.Contains("EventsAttendedYtd") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeEventsInvitedYtd()
//      {
//         return PropertiesToSave.Contains("EventsInvitedYtd") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpertise()
//      {
//         return PropertiesToSave.Contains("Expertise") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpressedConsentDaysSinceConsent()
//      {
//         return PropertiesToSave.Contains("ExpressedConsentDaysSinceConsent") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpressedConsentDaysUntilExpiry()
//      {
//         return PropertiesToSave.Contains("ExpressedConsentDaysUntilExpiry") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpressedConsentValid()
//      {
//         return PropertiesToSave.Contains("ExpressedConsentValid") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpressedContactConsent()
//      {
//         return PropertiesToSave.Contains("ExpressedContactConsent") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeExpressedContactConsentDate()
//      {
//         return PropertiesToSave.Contains("ExpressedContactConsentDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeFax()
//      {
//         return PropertiesToSave.Contains("Fax") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeFirmEmployee()
//      {
//         return PropertiesToSave.Contains("FirmEmployee") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeFirmRole()
//      {
//         return PropertiesToSave.Contains("FirmRole") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeFirstName()
//      {
//         return PropertiesToSave.Contains("FirstName") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeGender()
//      {
//         return PropertiesToSave.Contains("Gender") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeGloballyUnsubscribed()
//      {
//         return PropertiesToSave.Contains("GloballyUnsubscribed") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeGroupsUnsubscribedFrom()
//      {
//         return PropertiesToSave.Contains("GroupsUnsubscribedFrom") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeHireDate()
//      {
//         return PropertiesToSave.Contains("HireDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeHomePhone()
//      {
//         return PropertiesToSave.Contains("HomePhone") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeImpliedConsentDaysSinceConsent()
//      {
//         return PropertiesToSave.Contains("ImpliedConsentDaysSinceConsent") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeImpliedConsentDaysUntilExpiry()
//      {
//         return PropertiesToSave.Contains("ImpliedConsentDaysUntilExpiry") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeImpliedConsentValid()
//      {
//         return PropertiesToSave.Contains("ImpliedConsentValid") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeImpliedContactConsent()
//      {
//         return PropertiesToSave.Contains("ImpliedContactConsent") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeImpliedContactConsentDate()
//      {
//         return PropertiesToSave.Contains("ImpliedContactConsentDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeInternalUser()
//      {
//         return PropertiesToSave.Contains("InternalUser") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeIsAnEmployee()
//      {
//         return PropertiesToSave.Contains("IsAnEmployee") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeJobTitle()
//      {
//         return PropertiesToSave.Contains("JobTitle") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeKey()
//      {
//         return PropertiesToSave.Contains("Key") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLanguage()
//      {
//         return PropertiesToSave.Contains("Language") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLastActivity()
//      {
//         return PropertiesToSave.Contains("LastActivity") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLastActivityDate()
//      {
//         return PropertiesToSave.Contains("LastActivityDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLastName()
//      {
//         return PropertiesToSave.Contains("LastName") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLastWorkDate()
//      {
//         return PropertiesToSave.Contains("LastWorkDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeLinkedinUrl()
//      {
//         return PropertiesToSave.Contains("LinkedinUrl") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingAddress()
//      {
//         return PropertiesToSave.Contains("MailingAddress") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingCity()
//      {
//         return PropertiesToSave.Contains("MailingCity") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingCountry()
//      {
//         return PropertiesToSave.Contains("MailingCountry") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingPostalCode()
//      {
//         return PropertiesToSave.Contains("MailingPostalCode") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingStateProvince()
//      {
//         return PropertiesToSave.Contains("MailingStateProvince") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMailingStreetAddress()
//      {
//         return PropertiesToSave.Contains("MailingStreetAddress") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMarketingSubscriber()
//      {
//         return PropertiesToSave.Contains("MarketingSubscriber") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMemberships()
//      {
//         return PropertiesToSave.Contains("Memberships") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMiddleName()
//      {
//         return PropertiesToSave.Contains("MiddleName") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMissingCompanyName()
//      {
//         return PropertiesToSave.Contains("MissingCompanyName") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMissingEmail()
//      {
//         return PropertiesToSave.Contains("MissingEmail") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMissingFields()
//      {
//         return PropertiesToSave.Contains("MissingFields") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMissingJobTitle()
//      {
//         return PropertiesToSave.Contains("MissingJobTitle") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeMobilePhone()
//      {
//         return PropertiesToSave.Contains("MobilePhone") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeNetworkId()
//      {
//         return PropertiesToSave.Contains("NetworkId") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeNormalizedJobLevel()
//      {
//         return PropertiesToSave.Contains("NormalizedJobLevel") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeNormalizedJobTitle()
//      {
//         return PropertiesToSave.Contains("NormalizedJobTitle") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeNotes()
//      {
//         return PropertiesToSave.Contains("Notes") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeOfficeLocation()
//      {
//         return PropertiesToSave.Contains("OfficeLocation") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeOpenRate()
//      {
//         return PropertiesToSave.Contains("OpenRate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeOtherPhone()
//      {
//         return PropertiesToSave.Contains("OtherPhone") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePerson()
//      {
//         return PropertiesToSave.Contains("Person") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePersonId()
//      {
//         return PropertiesToSave.Contains("PersonId") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePersonalEmail()
//      {
//         return PropertiesToSave.Contains("PersonalEmail") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePhoto()
//      {
//         return PropertiesToSave.Contains("Photo") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePracticeArea()
//      {
//         return PropertiesToSave.Contains("PracticeArea") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePracticeAreas()
//      {
//         return PropertiesToSave.Contains("PracticeAreas") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePreferredName()
//      {
//         return PropertiesToSave.Contains("PreferredName") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePrefix()
//      {
//         return PropertiesToSave.Contains("Prefix") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePrimaryOffice()
//      {
//         return PropertiesToSave.Contains("PrimaryOffice") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeProfessionalCertifications()
//      {
//         return PropertiesToSave.Contains("ProfessionalCertifications") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializePublicationsSentYtd()
//      {
//         return PropertiesToSave.Contains("PublicationsSentYtd") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeRehireDate()
//      {
//         return PropertiesToSave.Contains("RehireDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeRelationshipCoOwners()
//      {
//         return PropertiesToSave.Contains("RelationshipCoOwners") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeRelationshipOwners()
//      {
//         return PropertiesToSave.Contains("RelationshipOwners") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeRelationshipsSignatureBlock()
//      {
//         return PropertiesToSave.Contains("RelationshipsSignatureBlock") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeSalutation()
//      {
//         return PropertiesToSave.Contains("Salutation") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeStartDate()
//      {
//         return PropertiesToSave.Contains("StartDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeStatus()
//      {
//         return PropertiesToSave.Contains("Status") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeSuffix()
//      {
//         return PropertiesToSave.Contains("Suffix") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTerminationDate()
//      {
//         return PropertiesToSave.Contains("TerminationDate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTest()
//      {
//         return PropertiesToSave.Contains("Test") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTimekeeperId()
//      {
//         return PropertiesToSave.Contains("TimekeeperId") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalClicks()
//      {
//         return PropertiesToSave.Contains("TotalClicks") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalCostOfAllEvents()
//      {
//         return PropertiesToSave.Contains("TotalCostOfAllEvents") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalEmailsAttempted()
//      {
//         return PropertiesToSave.Contains("TotalEmailsAttempted") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalEmailsReceived()
//      {
//         return PropertiesToSave.Contains("TotalEmailsReceived") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalEventsAttended()
//      {
//         return PropertiesToSave.Contains("TotalEventsAttended") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalEventsInvited()
//      {
//         return PropertiesToSave.Contains("TotalEventsInvited") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalOpens()
//      {
//         return PropertiesToSave.Contains("TotalOpens") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeTotalPublicationsSent()
//      {
//         return PropertiesToSave.Contains("TotalPublicationsSent") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeType()
//      {
//         return PropertiesToSave.Contains("Type") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeUniqueClicks()
//      {
//         return PropertiesToSave.Contains("UniqueClicks") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeUniqueOpens()
//      {
//         return PropertiesToSave.Contains("UniqueOpens") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeUnsubscribeAll()
//      {
//         return PropertiesToSave.Contains("UnsubscribeAll") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeUnsubscribeCount()
//      {
//         return PropertiesToSave.Contains("UnsubscribeCount") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeUnsubscribeRate()
//      {
//         return PropertiesToSave.Contains("UnsubscribeRate") && true;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCreatedBy()
//      {
//         return PropertiesToSave.Contains("CreatedBy") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeModifiedBy()
//      {
//         return PropertiesToSave.Contains("ModifiedBy") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeCreatedDate()
//      {
//         return PropertiesToSave.Contains("CreatedDate") && false;//true/false for if field is editable
//      }

//      public bool ShouldSerializeModifiedDate()
//      {
//         return PropertiesToSave.Contains("ModifiedDate") && false;//true/false for if field is editable
//      }

//   }
//}
