﻿using OnePlaceIntegrations.Core.Models;
using OnePlaceIntegrations.ClassBuilder.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnePlaceIntegrations.Core;
using System.Net.Http;
using Newtonsoft.Json;
using OnePlaceHelpers.ClassBuilder.Enums;

namespace OnePlaceIntegrations.ClassBuilder
{
   public class Factory
   {
      private RestClient client;
      private readonly Dictionary<long, string> fieldTypes = new Dictionary<long, string>()
      {
         {1, "Text" },
         {2, "Choice" },
         {3, "Number" },
         {4, "DateTime" },
         {5, "Reference"},
         {6, "YesNo"},
         {7, "User" },
         {16, "Photo" }
      };

      private readonly Dictionary<long, string> fieldTypeToVarType = new Dictionary<long, string>()
      {
         {1, "string" },
         {2, "string" },
         {3, "decimal?" },
         {4, "DateTime?" },
         {5, "string"},
         {6, "string"},
         {7, "string" },
         {16, "object" }
      };

      private readonly Dictionary<string, string> replaceDictionary = new Dictionary<string, string>
        {
            {"%", "Percent" },
            {"(s)", string.Empty },
            {"'s", string.Empty }
        };
      public Factory(RestClient client)
      {
         this.client = client;
      }

      public async Task Build(string outputDir = "./Generated/Models")
      {
         ObjectInfoResponse result = await client.GetAsync<ObjectInfoResponse>("api/beta/objects");
         List<ObjectInfo> objects = result.Model.Rows;
         //List<OPObjectField> objectFields = new List<OPObjectField>();
         Dictionary<string, string> classes = new Dictionary<string, string>();
         foreach (ObjectInfo obj in objects)
         {
            ObjectMetadataResponse metadataResponse = await client.GetAsync<ObjectMetadataResponse>("api/beta/objects/" + obj.Id);
            string generatedClass = BuildClassString(Utilities.GetCleanedName(metadataResponse.Model.SingularName, replaceDictionary), obj, metadataResponse.Model.Fields);
            if (!string.IsNullOrEmpty(generatedClass))
            {
               classes.Add(Utilities.GetCleanedName(metadataResponse.Model.SingularName, replaceDictionary), generatedClass);
            }
         }

         if (!Directory.Exists(outputDir))
         {
            Directory.CreateDirectory(outputDir);
         }

         foreach (string classKey in classes.Keys)
         {
            string path = Path.Combine(outputDir, classKey + ".cs");
            File.WriteAllText(path, classes[classKey]);
         }
         Console.WriteLine("test");

      }

      private string BuildClassString(string className, ObjectInfo obj, List<ObjectMetadataField> fields)
      {
         StringBuilder sb = new StringBuilder();
         sb.AppendLine("using System;");
         sb.AppendLine("using System.Collections.Generic;");
         sb.AppendLine("using System.Linq;");
         sb.AppendLine("using System.Text;");
         sb.AppendLine("using System.Threading.Tasks;");
         sb.AppendLine("using Newtonsoft.Json;");
         sb.AppendLine("using OnePlaceIntegrations.Core.Interfaces;");
         sb.AppendLine("namespace OnePlace.Models");
         sb.AppendLine("{");
         sb.AppendLine("   public class " + className + " : IOnePlaceObject");
         sb.AppendLine("   {");
         sb.AppendLine("");
         sb.AppendLine("    //This will be used allong with ShouldSerialize, so that you only send the data that you need to save and don't accidently delete or overwrite something.");
         sb.AppendLine("    //Add the name of the CSharpProperties that you wish to save. e.g. \"FirstName\"");
         sb.AppendLine("    [JsonIgnore]");
         sb.AppendLine("    public List<string> PropertiesToSave { get; } = new List<string>();");
         sb.AppendLine("    [JsonIgnore]");
         sb.AppendLine("     public string ListName { get; } = \"" + obj.Name + "\";");
         sb.AppendLine("    [JsonIgnore]");
         sb.AppendLine("     public long ListId { get; } = " + obj.Id + ";");
         sb.AppendLine("    [JsonProperty(\"id\")]");
         sb.AppendLine("    public long? Id { get; set; }");
         sb.AppendLine("    [JsonProperty(\"reference ids\")]");
         sb.AppendLine("    public Dictionary<string, long> ReferenceIds { get; set;} = new Dictionary<string, long>();");
         sb.AppendLine("    [JsonProperty(\"links\")]");
         sb.AppendLine("    public Dictionary<string, string> Links { get; set; } = new Dictionary<string, string>();");
         foreach (ObjectMetadataField field in fields)
         {
            sb.Append("      ");
            sb.AppendLine(GetFieldString(className, field));
            if (field.FieldType == (int)FieldTypeEnum.Choice)// && obj.Id == 2011)
            {
               sb.AppendLine(GetChoiceFieldOptionsString(className, field));
            }
         }

         sb.AppendLine("//These methods are used by the One Place Client to send only the data that is updated, using the PropertiesToSave list.");
         foreach (ObjectMetadataField field in fields)
         {
            sb.AppendLine(GetSerializerMethod(Utilities.GetCsharpProperty(className, field, replaceDictionary), field.IsEditable));
         }
         sb.AppendLine("   }");
         sb.AppendLine("}");

         return sb.ToString();
      }

      private string GetSerializerMethod(string csharpProp, bool? isEditable)
      {
         StringBuilder sb = new StringBuilder();
         sb.AppendLine("      public bool ShouldSerialize" + csharpProp + "() {");
         sb.AppendLine($"        return PropertiesToSave.Contains(\"{csharpProp}\") && " + ((isEditable.HasValue && isEditable.Value) ? "true;" : "false;") + "//true/false for if field is editable");
         sb.AppendLine("     }");
         return sb.ToString();
      }

      private string GetFieldString(string className, ObjectMetadataField field)
      {
         if (string.IsNullOrEmpty(field.Name)) return "";
         StringBuilder sb = new StringBuilder();
         string varType;
         string jsonProp = Utilities.GetJsonProperty(className, field);
         string csharpProp = Utilities.GetCsharpProperty(className, field, replaceDictionary);
         fieldTypeToVarType.TryGetValue(field.FieldType.Value, out varType);
         varType = string.IsNullOrEmpty(varType) ? "string" : varType;
         sb.AppendLine($"[JsonProperty(\"{jsonProp}\")]");
         sb.Append("      public ");
         sb.Append(varType);
         sb.Append(" " + csharpProp);
         sb.AppendLine(" { get; set; }");


         return sb.ToString();
      }

      private string GetChoiceFieldOptionsString(string className, ObjectMetadataField field)
      {
         StringBuilder sb = new StringBuilder();
         string csharpProp = Utilities.GetCsharpProperty(className, field, replaceDictionary) + "ChoiceOptions";
         sb.AppendLine("[JsonIgnore]");
         sb.Append("public Dictionary<string, long?> ");
         sb.Append(csharpProp);
         sb.Append(" = new Dictionary<string, long?>() {");
         int counter = 0;
         foreach (ChoiceFieldOption option in field.ChoiceFieldOptions)
         {
            if (counter > 0) sb.Append(",");
            sb.Append("{\"");
            sb.Append(option.Name.Replace("\n", "\\n"));
            sb.Append("\"");
            sb.Append(",");
            sb.Append(option.Id);
            sb.Append("}");
            counter++;
         }
         sb.Append("};");
         return sb.ToString();
      }
   }
}
