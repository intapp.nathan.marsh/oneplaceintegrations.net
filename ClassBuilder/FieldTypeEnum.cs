﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceHelpers.ClassBuilder.Enums
{
   public enum FieldTypeEnum
   {
      Text = 1,
      Choice = 2,
      Number = 3,
      DateTime = 4,
      Reference = 5,
      YesNo = 6,
      User = 7,
      Photo = 16
   }
}
