﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace OnePlaceIntegrations.ClassBuilder.Models
{
   public partial class ObjectEntryField
   {
      [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
      public List<ObjectEntryFieldData> Model { get; set; }

      [JsonProperty("statusCode", NullValueHandling = NullValueHandling.Ignore)]
      public long? StatusCode { get; set; }

      [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
      public string Message { get; set; }
   }

   public partial class ObjectEntryFieldData
   {
      [JsonProperty("indentLevel", NullValueHandling = NullValueHandling.Ignore)]
      public long? IndentLevel { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }

      [JsonProperty("formFieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormFieldType { get; set; }

      [JsonProperty("isRequired", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsRequired { get; set; }

      [JsonProperty("isName", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsName { get; set; }

      [JsonProperty("isMultiple", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMultiple { get; set; }

      [JsonProperty("value")]
      public ObjectEntryFieldValue? Value { get; set; }

      [JsonProperty("isDataEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDataEditable { get; set; }

      [JsonProperty("isContext", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsContext { get; set; }

      [JsonProperty("isBulkEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsBulkEditable { get; set; }

      [JsonProperty("isDocumentData", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDocumentData { get; set; }

      [JsonProperty("isEditForbidden", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsEditForbidden { get; set; }

      [JsonProperty("systemFieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? SystemFieldType { get; set; }

      [JsonProperty("isWarnOnNearDuplicates", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsWarnOnNearDuplicates { get; set; }

      [JsonProperty("isNearDupIgnored", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsNearDupIgnored { get; set; }

      [JsonProperty("isCalculated", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsCalculated { get; set; }

      [JsonProperty("isDuplicatesAllowed", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDuplicatesAllowed { get; set; }

      [JsonProperty("fieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FieldType { get; set; }

      [JsonProperty("isDataDBWritable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDataDbWritable { get; set; }

      [JsonProperty("entryListsIds", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> EntryListsIds { get; set; }

      [JsonProperty("isForceStepByStep", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsForceStepByStep { get; set; }

      [JsonProperty("choiceFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ChoiceFieldId { get; set; }

      [JsonProperty("choiceOrder", NullValueHandling = NullValueHandling.Ignore)]
      public long? ChoiceOrder { get; set; }

      [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
      public string Description { get; set; }

      [JsonProperty("numberFormatType", NullValueHandling = NullValueHandling.Ignore)]
      public long? NumberFormatType { get; set; }

      [JsonProperty("formatTypeId", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormatTypeId { get; set; }

      [JsonProperty("defaultValue", NullValueHandling = NullValueHandling.Ignore)]
      public ObjectEntryFieldDefaultValueUnion? DefaultValue { get; set; }

      [JsonProperty("numberType", NullValueHandling = NullValueHandling.Ignore)]
      public long? NumberType { get; set; }

      [JsonProperty("hasTime", NullValueHandling = NullValueHandling.Ignore)]
      public bool? HasTime { get; set; }

      [JsonProperty("isCalculationOverridden", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsCalculationOverridden { get; set; }
   }

   public partial class DefaultValueClass
   {
      [JsonProperty("seqNumber", NullValueHandling = NullValueHandling.Ignore)]
      public long? SeqNumber { get; set; }

      [JsonProperty("isAutoPdf", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsAutoPdf { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }

      [JsonProperty("entryListId", NullValueHandling = NullValueHandling.Ignore)]
      public long? EntryListId { get; set; }
   }

   public partial struct ObjectEntryFieldDefaultValueUnion
   {
      public bool? Bool;
      public DefaultValueClass DefaultValueClass;

      public static implicit operator ObjectEntryFieldDefaultValueUnion(bool Bool) => new ObjectEntryFieldDefaultValueUnion { Bool = Bool };
      public static implicit operator ObjectEntryFieldDefaultValueUnion(DefaultValueClass DefaultValueClass) => new ObjectEntryFieldDefaultValueUnion { DefaultValueClass = DefaultValueClass };
   }

   public partial struct ObjectEntryFieldValue
   {
      public bool? Bool;
      public string String;

      public static implicit operator ObjectEntryFieldValue(bool Bool) => new ObjectEntryFieldValue { Bool = Bool };
      public static implicit operator ObjectEntryFieldValue(string String) => new ObjectEntryFieldValue { String = String };
      public bool IsNull => Bool == null && String == null;
   }

   internal static class ObjectEntryFieldConverter
   {
      public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
      {
         MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
         DateParseHandling = DateParseHandling.None,
         Converters =
            {
                DefaultValueUnionConverter.Singleton,
                ObjectEntryFieldDefaultValueConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
      };
   }

   internal class DefaultValueUnionConverter : JsonConverter
   {
      public override bool CanConvert(Type t) => t == typeof(ObjectEntryFieldDefaultValueUnion) || t == typeof(ObjectEntryFieldDefaultValueUnion?);

      public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
      {
         switch (reader.TokenType)
         {
            case JsonToken.Boolean:
               var boolValue = serializer.Deserialize<bool>(reader);
               return new ObjectEntryFieldDefaultValueUnion { Bool = boolValue };
            case JsonToken.StartObject:
               var objectValue = serializer.Deserialize<DefaultValueClass>(reader);
               return new ObjectEntryFieldDefaultValueUnion { DefaultValueClass = objectValue };
         }
         throw new Exception("Cannot unmarshal type DefaultValueUnion");
      }

      public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
      {
         var value = (ObjectEntryFieldDefaultValueUnion)untypedValue;
         if (value.Bool != null)
         {
            serializer.Serialize(writer, value.Bool.Value);
            return;
         }
         if (value.DefaultValueClass != null)
         {
            serializer.Serialize(writer, value.DefaultValueClass);
            return;
         }
         throw new Exception("Cannot marshal type DefaultValueUnion");
      }

      public static readonly DefaultValueUnionConverter Singleton = new DefaultValueUnionConverter();
   }

   internal class ObjectEntryFieldDefaultValueConverter : JsonConverter
   {
      public override bool CanConvert(Type t) => t == typeof(ObjectEntryFieldValue) || t == typeof(ObjectEntryFieldValue?);

      public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
      {
         switch (reader.TokenType)
         {
            case JsonToken.Null:
               return new ObjectEntryFieldValue { };
            case JsonToken.Boolean:
               var boolValue = serializer.Deserialize<bool>(reader);
               return new ObjectEntryFieldValue { Bool = boolValue };
            case JsonToken.String:
            case JsonToken.Date:
               var stringValue = serializer.Deserialize<string>(reader);
               return new ObjectEntryFieldValue { String = stringValue };
         }
         throw new Exception("Cannot unmarshal type Value");
      }

      public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
      {
         var value = (ObjectEntryFieldValue)untypedValue;
         if (value.IsNull)
         {
            serializer.Serialize(writer, null);
            return;
         }
         if (value.Bool != null)
         {
            serializer.Serialize(writer, value.Bool.Value);
            return;
         }
         if (value.String != null)
         {
            serializer.Serialize(writer, value.String);
            return;
         }
         throw new Exception("Cannot marshal type Value");
      }

      public static readonly ObjectEntryFieldDefaultValueConverter Singleton = new ObjectEntryFieldDefaultValueConverter();
   }
}
