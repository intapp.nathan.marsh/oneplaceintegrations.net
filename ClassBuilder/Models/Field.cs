﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


namespace OnePlaceIntegrations.ClassBuilder.Models
{
   public partial class ObjectField
   {
      [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
      public List<ObjectFieldData> Model { get; set; }

      [JsonProperty("statusCode", NullValueHandling = NullValueHandling.Ignore)]
      public long? StatusCode { get; set; }

      [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
      public string Message { get; set; }
   }

   public partial class ObjectFieldData
   {
      [JsonProperty("uniqueId", NullValueHandling = NullValueHandling.Ignore)]
      public long? UniqueId { get; set; }

      [JsonProperty("fieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FieldType { get; set; }

      [JsonProperty("fieldFormatType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FieldFormatType { get; set; }

      [JsonProperty("referenceFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ReferenceFieldId { get; set; }

      [JsonProperty("listId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ListId { get; set; }

      [JsonProperty("index", NullValueHandling = NullValueHandling.Ignore)]
      public long? Index { get; set; }

      [JsonProperty("notificationDisplayType", NullValueHandling = NullValueHandling.Ignore)]
      public long? NotificationDisplayType { get; set; }

      [JsonProperty("notificationDisplayOrder", NullValueHandling = NullValueHandling.Ignore)]
      public long? NotificationDisplayOrder { get; set; }

      [JsonProperty("enableAutoPdf", NullValueHandling = NullValueHandling.Ignore)]
      public bool? EnableAutoPdf { get; set; }

      [JsonProperty("isMultiSelect", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMultiSelect { get; set; }

      [JsonProperty("isRichText", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsRichText { get; set; }

      [JsonProperty("systemFieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? SystemFieldType { get; set; }

      [JsonProperty("textFormatType", NullValueHandling = NullValueHandling.Ignore)]
      public long? TextFormatType { get; set; }

      [JsonProperty("isMoney", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMoney { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }
}
