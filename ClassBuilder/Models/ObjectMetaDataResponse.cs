﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.ClassBuilder.Models
{
   public partial class ObjectMetadataResponse
   {
      [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
      public ObjectMetadata Model { get; set; }

      [JsonProperty("statusCode", NullValueHandling = NullValueHandling.Ignore)]
      public long? StatusCode { get; set; }

      [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
      public string Message { get; set; }
   }

   public partial class ObjectMetadata
   {
      [JsonProperty("singularName", NullValueHandling = NullValueHandling.Ignore)]
      public string SingularName { get; set; }

      [JsonProperty("pluralName", NullValueHandling = NullValueHandling.Ignore)]
      public string PluralName { get; set; }

      [JsonProperty("entryAvailabilityType", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> EntryAvailabilityType { get; set; }

      [JsonProperty("isInQuickSearch", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsInQuickSearch { get; set; }

      [JsonProperty("isEntryLevelCapabilities", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsEntryLevelCapabilities { get; set; }

      [JsonProperty("isUserFieldSelection", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUserFieldSelection { get; set; }

      [JsonProperty("isInheritedCapabilities", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsInheritedCapabilities { get; set; }

      [JsonProperty("isShownInNavigation", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsShownInNavigation { get; set; }

      [JsonProperty("hasDashboard", NullValueHandling = NullValueHandling.Ignore)]
      public bool? HasDashboard { get; set; }

      [JsonProperty("fields", NullValueHandling = NullValueHandling.Ignore)]
      public List<ObjectMetadataField> Fields { get; set; }

      [JsonProperty("sourceEntryListId", NullValueHandling = NullValueHandling.Ignore)]
      public long? SourceEntryListId { get; set; }

      [JsonProperty("fieldTemplates", NullValueHandling = NullValueHandling.Ignore)]
      public List<ObjectMetadatFieldTemplate> FieldTemplates { get; set; }

      [JsonProperty("isInFullSearch", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsInFullSearch { get; set; }

      [JsonProperty("entryFormId", NullValueHandling = NullValueHandling.Ignore)]
      public long? EntryFormId { get; set; }

      [JsonProperty("entryForm", NullValueHandling = NullValueHandling.Ignore)]
      public EntryForm EntryForm { get; set; }

      [JsonProperty("isDeletable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDeletable { get; set; }

      [JsonProperty("isEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsEditable { get; set; }

      [JsonProperty("entryListSubType", NullValueHandling = NullValueHandling.Ignore)]
      public long? EntryListSubType { get; set; }

      [JsonProperty("entryListType", NullValueHandling = NullValueHandling.Ignore)]
      public long? EntryListType { get; set; }

      [JsonProperty("isCreateEntriesDynamically", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsCreateEntriesDynamically { get; set; }

      [JsonProperty("isVLookupMapping", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsVLookupMapping { get; set; }

      [JsonProperty("goToDetailPageOnCreateNew", NullValueHandling = NullValueHandling.Ignore)]
      public bool? GoToDetailPageOnCreateNew { get; set; }

      [JsonProperty("isUsedAsSubmissionList", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUsedAsSubmissionList { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }

   public partial class EntryForm
   {
      [JsonProperty("tabs", NullValueHandling = NullValueHandling.Ignore)]
      public List<Tab> Tabs { get; set; }

      [JsonProperty("grids", NullValueHandling = NullValueHandling.Ignore)]
      public List<Grid> Grids { get; set; }

      [JsonProperty("choiceId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ChoiceId { get; set; }

      [JsonProperty("filterFieldsIds", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> FilterFieldsIds { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }
   }

   public partial class Grid
   {
      [JsonProperty("entryListId", NullValueHandling = NullValueHandling.Ignore)]
      public long? EntryListId { get; set; }

      [JsonProperty("relationshipFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? RelationshipFieldId { get; set; }

      [JsonProperty("fieldIds", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> FieldIds { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }

   public partial class Tab
   {
      [JsonProperty("choiceValues", NullValueHandling = NullValueHandling.Ignore)]
      public List<object> ChoiceValues { get; set; }

      [JsonProperty("fields", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> Fields { get; set; }

      [JsonProperty("filters", NullValueHandling = NullValueHandling.Ignore)]
      public List<ObjectMetadataFilter> Filters { get; set; }

      [JsonProperty("cascadingFieldIdsByParentId", NullValueHandling = NullValueHandling.Ignore)]
      public CascadingFieldIdsByParentId CascadingFieldIdsByParentId { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }

   public partial class CascadingFieldIdsByParentId
   {
      [JsonProperty("2186", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> The2186 { get; set; }
   }

   public partial class ObjectMetadataFilter
   {
      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("values", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> Values { get; set; }
   }

   public partial class ObjectMetadatFieldTemplate
   {
      [JsonProperty("templateId", NullValueHandling = NullValueHandling.Ignore)]
      public long? TemplateId { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }

   public partial class ObjectMetadataField
   {
      [JsonProperty("isEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsEditable { get; set; }

      [JsonProperty("isDeletable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDeletable { get; set; }

      [JsonProperty("isDeletableInitial", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDeletableInitial { get; set; }

      [JsonProperty("isUsedInLookups", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUsedInLookups { get; set; }

      [JsonProperty("isUsedInNotifications", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUsedInNotifications { get; set; }

      [JsonProperty("isUsedInLookupsEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUsedInLookupsEditable { get; set; }

      [JsonProperty("isUsedInWorkflows", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsUsedInWorkflows { get; set; }

      [JsonProperty("isRequired", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsRequired { get; set; }

      [JsonProperty("isChanged", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsChanged { get; set; }

      [JsonProperty("isMultiselect", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMultiselect { get; set; }

      [JsonProperty("order", NullValueHandling = NullValueHandling.Ignore)]
      public long? Order { get; set; }

      [JsonProperty("formatTypeId", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormatTypeId { get; set; }

      [JsonProperty("formatSubTypeId", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormatSubTypeId { get; set; }

      [JsonProperty("sourceTypeId", NullValueHandling = NullValueHandling.Ignore)]
      public long? SourceTypeId { get; set; }

      [JsonProperty("choiceStateTransitions", NullValueHandling = NullValueHandling.Ignore)]
      public List<object> ChoiceStateTransitions { get; set; }

      [JsonProperty("isStepByStepEnforced", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsStepByStepEnforced { get; set; }

      [JsonProperty("isName", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsName { get; set; }

      [JsonProperty("isKey", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsKey { get; set; }

      [JsonProperty("references", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> References { get; set; }

      [JsonProperty("isReferencesEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsReferencesEditable { get; set; }

      [JsonProperty("isCalculated", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsCalculated { get; set; }

      [JsonProperty("isSmartField", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsSmartField { get; set; }

      [JsonProperty("formula", NullValueHandling = NullValueHandling.Ignore)]
      public string Formula { get; set; }

      [JsonProperty("templateId", NullValueHandling = NullValueHandling.Ignore)]
      public long? TemplateId { get; set; }

      [JsonProperty("formulaType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormulaType { get; set; }

      [JsonProperty("isAttachment", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsAttachment { get; set; }

      [JsonProperty("systemFieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? SystemFieldType { get; set; }

      [JsonProperty("allowDuplicates", NullValueHandling = NullValueHandling.Ignore)]
      public bool? AllowDuplicates { get; set; }

      [JsonProperty("warnNearDuplicates", NullValueHandling = NullValueHandling.Ignore)]
      public bool? WarnNearDuplicates { get; set; }

      [JsonProperty("isInheritDataOnNew", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsInheritDataOnNew { get; set; }

      [JsonProperty("excludedFromInheritData", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> ExcludedFromInheritData { get; set; }

      [JsonProperty("isDataEditable", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDataEditable { get; set; }

      [JsonProperty("userGroupFilters", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> UserGroupFilters { get; set; }

      [JsonProperty("isDisplayInactiveUsers", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDisplayInactiveUsers { get; set; }

      [JsonProperty("uniqueId", NullValueHandling = NullValueHandling.Ignore)]
      public long? UniqueId { get; set; }

      [JsonProperty("fieldType", NullValueHandling = NullValueHandling.Ignore)]
      public long? FieldType { get; set; }

      [JsonProperty("referenceFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ReferenceFieldId { get; set; }

      [JsonProperty("listId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ListId { get; set; }

      [JsonProperty("index", NullValueHandling = NullValueHandling.Ignore)]
      public long? Index { get; set; }

      [JsonProperty("notificationDisplayType", NullValueHandling = NullValueHandling.Ignore)]
      public long? NotificationDisplayType { get; set; }

      [JsonProperty("notificationDisplayOrder", NullValueHandling = NullValueHandling.Ignore)]
      public long? NotificationDisplayOrder { get; set; }

      [JsonProperty("isAllowOverride", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsAllowOverride { get; set; }

      [JsonProperty("enableAutoPdf", NullValueHandling = NullValueHandling.Ignore)]
      public bool? EnableAutoPdf { get; set; }

      [JsonProperty("isMultiSelect", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMultiSelect { get; set; }

      [JsonProperty("isRichText", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsRichText { get; set; }

      [JsonProperty("textFormatType", NullValueHandling = NullValueHandling.Ignore)]
      public long? TextFormatType { get; set; }

      [JsonProperty("isMoney", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsMoney { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }

      [JsonProperty("referencesType", NullValueHandling = NullValueHandling.Ignore)]
      public long? ReferencesType { get; set; }

      [JsonProperty("choiceFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? ChoiceFieldId { get; set; }

      [JsonProperty("smartFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? SmartFieldId { get; set; }

      [JsonProperty("choiceFieldOrder", NullValueHandling = NullValueHandling.Ignore)]
      public long? ChoiceFieldOrder { get; set; }

      [JsonProperty("choiceFieldOptions", NullValueHandling = NullValueHandling.Ignore)]
      public List<ChoiceFieldOption> ChoiceFieldOptions { get; set; }

      [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
      public string Description { get; set; }

      [JsonProperty("defaultValue", NullValueHandling = NullValueHandling.Ignore)]
      public long? DefaultValue { get; set; }

      [JsonProperty("inputEntryListId", NullValueHandling = NullValueHandling.Ignore)]
      public long? InputEntryListId { get; set; }

      [JsonProperty("inputFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? InputFieldId { get; set; }

      [JsonProperty("formulaOperatorId", NullValueHandling = NullValueHandling.Ignore)]
      public long? FormulaOperatorId { get; set; }

      [JsonProperty("groupByFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? GroupByFieldId { get; set; }

      [JsonProperty("smartFieldTypeId", NullValueHandling = NullValueHandling.Ignore)]
      public long? SmartFieldTypeId { get; set; }

      [JsonProperty("stageFieldId", NullValueHandling = NullValueHandling.Ignore)]
      public long? StageFieldId { get; set; }

      [JsonProperty("stageFieldValueIds", NullValueHandling = NullValueHandling.Ignore)]
      public List<long> StageFieldValueIds { get; set; }
   }

   public partial class ChoiceFieldOption
   {
      [JsonProperty("order", NullValueHandling = NullValueHandling.Ignore)]
      public long? Order { get; set; }

      [JsonProperty("isAutoPdf", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsAutoPdf { get; set; }

      [JsonProperty("isDisabledRemove", NullValueHandling = NullValueHandling.Ignore)]
      public bool? IsDisabledRemove { get; set; }

      [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
      public long? Id { get; set; }

      [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
      public string Name { get; set; }
   }
}



