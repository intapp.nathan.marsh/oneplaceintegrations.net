﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnePlaceIntegrations.ClassBuilder.Models
{
   public partial class ObjectInfoResponse
   {
      [JsonProperty("model")]
      public ObjectInfoData Model { get; set; }

      [JsonProperty("statusCode")]
      public long StatusCode { get; set; }

      [JsonProperty("message")]
      public string Message { get; set; }
   }

   public partial class ObjectInfoData
   {
      [JsonProperty("totalRecords")]
      public long TotalRecords { get; set; }

      [JsonProperty("totalPages")]
      public long TotalPages { get; set; }

      [JsonProperty("rows")]
      public List<ObjectInfo> Rows { get; set; }
   }

   public partial class ObjectInfo
   {
      [JsonProperty("category")]
      public string Category { get; set; }

      [JsonProperty("count")]
      public long Count { get; set; }

      [JsonProperty("isDeletable")]
      public bool IsDeletable { get; set; }

      [JsonProperty("isEditable")]
      public bool IsEditable { get; set; }

      [JsonProperty("entryListType")]
      public long EntryListType { get; set; }

      [JsonProperty("id")]
      public long Id { get; set; }

      [JsonProperty("name")]
      public string Name { get; set; }
   }

   //public enum ObjectInfoCategory { Activity, Aggregation, Document, Entity, Person, Task };

   //internal static class Converter
   //{
   //   public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
   //   {
   //      MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
   //      DateParseHandling = DateParseHandling.None,
   //      Converters =
   //         {
   //             CategoryConverter.Singleton,
   //             new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
   //         },
   //   };
   //}

   //internal class CategoryConverter : JsonConverter
   //{
   //   public override bool CanConvert(Type t) => t == typeof(ObjectInfoCategory) || t == typeof(ObjectInfoCategory?);

   //   public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
   //   {
   //      if (reader.TokenType == JsonToken.Null) return null;
   //      var value = serializer.Deserialize<string>(reader);
   //      switch (value)
   //      {
   //         case "Activity":
   //            return ObjectInfoCategory.Activity;
   //         case "Aggregation":
   //            return ObjectInfoCategory.Aggregation;
   //         case "Document":
   //            return ObjectInfoCategory.Document;
   //         case "Entity":
   //            return ObjectInfoCategory.Entity;
   //         case "Person":
   //            return ObjectInfoCategory.Person;
   //         case "Task":
   //            return ObjectInfoCategory.Task;
   //      }
   //      throw new Exception("Cannot unmarshal type Category");
   //   }

   //   public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
   //   {
   //      if (untypedValue == null)
   //      {
   //         serializer.Serialize(writer, null);
   //         return;
   //      }
   //      var value = (ObjectInfoCategory)untypedValue;
   //      switch (value)
   //      {
   //         case ObjectInfoCategory.Activity:
   //            serializer.Serialize(writer, "Activity");
   //            return;
   //         case ObjectInfoCategory.Aggregation:
   //            serializer.Serialize(writer, "Aggregation");
   //            return;
   //         case ObjectInfoCategory.Document:
   //            serializer.Serialize(writer, "Document");
   //            return;
   //         case ObjectInfoCategory.Entity:
   //            serializer.Serialize(writer, "Entity");
   //            return;
   //         case ObjectInfoCategory.Person:
   //            serializer.Serialize(writer, "Person");
   //            return;
   //         case ObjectInfoCategory.Task:
   //            serializer.Serialize(writer, "Task");
   //            return;
   //      }
   //      throw new Exception("Cannot marshal type Category");
   //   }

   //   public static readonly CategoryConverter Singleton = new CategoryConverter();
   //}
}
