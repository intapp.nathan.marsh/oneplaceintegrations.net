﻿#region using
using OnePlaceIntegrations.Core;
using OnePlaceIntegrations.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace OnePlaceHelpers.Helpers
{
   public static class PropertyHelper
   {

      #region methods
      public static void AddAllExceptNull(IOnePlaceObject obj)
      {
         RemoveAll(obj);
         PropertyInfo[] properties = obj.GetType().GetProperties();
         foreach (PropertyInfo prop in properties)
         {
            if (!Utilities.IsNullOrEmpty(obj.GetType().GetProperty(prop.Name).GetValue(obj, null)))
            {
               obj.PropertiesToSave.Add(prop.Name);
            }

         }
      }

      public static void AddAllIncludingNull(IOnePlaceObject obj)
      {
         RemoveAll(obj);
         PropertyInfo[] properties = obj.GetType().GetProperties();
         foreach (PropertyInfo prop in properties)
         {
            obj.PropertiesToSave.Add(prop.Name);
         }
      }

      public static void AddOnlyNull(IOnePlaceObject obj)
      {
         RemoveAll(obj);
         PropertyInfo[] properties = obj.GetType().GetProperties();
         foreach (PropertyInfo prop in properties)
         {
            if (Utilities.IsNullOrEmpty(obj.GetType().GetProperty(prop.Name).GetValue(obj, null)))
            {
               obj.PropertiesToSave.Add(prop.Name);
            }

         }
      }

      public static void RemoveAll(IOnePlaceObject obj)
      {
         obj.PropertiesToSave.Clear();
      }

      #endregion
   }
}
