﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using OnePlace.Models;
using OnePlaceIntegrations;
using OnePlaceIntegrations.ClassBuilder;

namespace OnePlaceTest
{
   class Program
   {
      static async Task Main(string[] args)
      {
         try
         {
            string clientId = ConfigurationManager.AppSettings["OP_CLIENT_ID"];
            string clientSecret = ConfigurationManager.AppSettings["OP_CLIENT_SECRET"];
            string baseUrl = ConfigurationManager.AppSettings["OP_URL"];
            OnePlaceIntegrations.Core.RestClient onePlaceClient = new OnePlaceIntegrations.Core.RestClient(baseUrl, clientId, clientSecret);
            Factory classBuilder = new Factory(onePlaceClient);
             await classBuilder.Build();

            //List<Contact> contacts = await onePlaceClient.GetAllObjectDataAsync<Contact>();

            //Contact contact = contacts.Find(x=> x.Id == 3276);
            //contact.Status = "3265";
            //contact.Id = 3276;
            //contact.FirstName = "Nathan";
            //contact.LastName = "Test 10.3";
            //contact.Email = "nathantest10@intapp.com";

            ////contact.PropertiesToSave.Add("FirstName");
            //contact.PropertiesToSave.Add("LastName");
            ////contact.PropertiesToSave.Add("Email");
            //contact.PropertiesToSave.Add("Status");
            //var results = await onePlaceClient.UpdateObjectDataAsync<Contact>(new List<Contact>() { contact });
            Console.WriteLine("test");
         }
         catch (Exception e)
         {
            Console.WriteLine(e);
         }

      }
   }
}
